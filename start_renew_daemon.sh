#!/bin/bash

###############
# Load config #
###############
if [ ! -f ./config.env ]; then
    echo "Config file not found! See README.md"
    exit 1
fi
if [ -z "$WITH_ENV" ]; then
    export WITH_ENV=true
    # Relaunch script with config loaded
    exec bash -ac ". ./config.env; $0"
fi


# Build the image
RENEW_DAEMON_IMAGE=letsencrypt-renew-daemon
docker build \
       -t $RENEW_DAEMON_IMAGE \
       -f RenewDaemonDockerfile \
       . > /dev/null

# Start the daemon
IN_CONTAINER_LETSENCRYPT="/etc/letsencrypt/"
IN_CONTAINER_WEBROOT_CHALLENGE="/webroot_challenge/"
docker run \
       -d \
       --restart=always \
       -v "$WEBROOT:$IN_CONTAINER_WEBROOT_CHALLENGE" \
       -v "$LETSENCRYPT:$IN_CONTAINER_LETSENCRYPT" \
       --name=$RENEW_DAEMON_CONTAINER_NAME \
       $RENEW_DAEMON_IMAGE \
       > /dev/null

if [ $? == '0' ]; then
    echo "Renew Daemon started under the name: \"$RENEW_DAEMON_CONTAINER_NAME\""
else
    echo "Problem starting the Renew Daemon"
fi
