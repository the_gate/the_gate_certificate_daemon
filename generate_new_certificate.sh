#!/bin/bash

###############
# Load config #
###############
if [ ! -f ./config.env ]; then
    echo "Config file not found! See README.md"
    exit 1
fi
if [ -z "$WITH_ENV" ]; then
    export WITH_ENV=true
    # Relaunch script with config loaded
    exec bash -ac ". ./config.env; $0"
fi



#############
# Functions #
#############
function print_intro {
    echo "####################################################################"
    echo "##                                                                ##"
    echo "## /!\     THIS WILL REMOVE YOUR EXISTING CERTIFICATES        /!\ ##"
    echo "## /!\ AND COMPLETELY ERASE YOUR LETSENCRYPT CONFIGURATION !! /!\ ##"
    echo "##                                                                ##"
    echo "##      If you want to RENEW the certificates:                    ##"
    echo "##             _____________________________                      ##"
    echo "##             |                           |                      ##"
    echo "##             | -> Start the Renew daemon |                      ##"
    echo "##             |___________________________|                      ##"
    echo "##                                                                ##"
    echo "##                                                                ##"
    echo "####################################################################"
    echo "##"
    read -p "## Are you sure? [y/N]" -n 1 -r
    echo
    echo "##"
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
        echo "## ABORTED"
        echo "##########################"
        exit 1
    fi
    echo "##########################"
    echo "## GENERATE CERTIFICATE ##"
    if [ "$1" == "debug" ]; then
        echo
        echo "Cmd to be run in container:"
        echo "certbot $CERTBOT_ARGS"
    fi
    echo "##########################"

}
function print_success {
    # Delete largest substring that match `[ ]*` from end of string.
    #  - largest: 2 times `%` == `%%`. 1 time is delete smallest.
    #  - from end: `%` instead of `#`. `#` deletes from front.
    FIRST_DOMAIN=${DOMAINS%%[ ]*}

    echo
    echo "############################################################################"
    echo "##"
    echo "##   CERTIFICATE CREATION SUCCESSFUL !!"
    echo "##   =================================="
    echo "##"
    echo "##   Your certificates are stored in:"
    echo "##   \`${LETSENCRYPT}live/$FIRST_DOMAIN/\`"
    echo "##"
    echo "##   CERTIFICATE:"
    echo "##   \`${LETSENCRYPT}live/$FIRST_DOMAIN/fullchain.pem\`"
    echo "##"
    echo "##   PRIVATE KEY:"
    echo "##   \`${LETSENCRYPT}live/$FIRST_DOMAIN/privkey.pem\`"
    echo "##"
    echo "############################################################################"
    echo
}
function print_failure {
    echo
    echo "#####################################"
    echo "## CERTIFICATE CREATION FAILED !!  ##"
    echo "#####################################"
    echo
}

function run_in_certbot_container {
    docker run -it --rm \
           -v "$WEBROOT:$IN_CONTAINER_WEBROOT_CHALLENGE" \
           -v "$LETSENCRYPT:$IN_CONTAINER_LETSENCRYPT" \
           --entrypoint "" \
           certbot/certbot $@
}


########
# Main #
########
# Set container directories locations
IN_CONTAINER_LETSENCRYPT="/etc/letsencrypt/"
IN_CONTAINER_WEBROOT_CHALLENGE="/webroot_challenge/"

# Build `certbot` command
CERTBOT_ARGS="certonly --webroot -w $IN_CONTAINER_WEBROOT_CHALLENGE"
for domain in ${DOMAINS[@]}; do
    CERTBOT_ARGS=$CERTBOT_ARGS" -d $domain"
done

# Use `print_intro debug` to debug the `certbot` command built at the previous step
print_intro

# Remove ALL existing `letsencrypt` configuration
run_in_certbot_container rm -rf $IN_CONTAINER_LETSENCRYPT > /dev/null

# Run certbot in container
run_in_certbot_container certbot $CERTBOT_ARGS

if [ $? == '0' ]; then
    print_success
else
    print_failure
fi
