# Letsencrypt Docker Daemon

# Deprecated - See: [Certificate Tutorial in **The Gate**](https://shockn745.github.io/The-Gate/#extra-generating-certificates-with-lets-encrypt-and-certbot)

This docker image takes care of SSL certificate creation and renewal using letsencrypt and `certbot`.

## What does it do?

It generates **ONE** certificate and **ONE** key-pair for **one or multiple** domains.

### Remote Docker socket

This script **CAN** be executed on a remote Docker socket.

**It will:**
- Generate certificates on the **Remote Server**
- Store the certificates on the **Remote Server**

## Instructions

### Prequisites
#### Serving static content on the `webroot`
For each domain you must be serving static content under: `http://YOURDOMAIN/.well-known/`.
The `root directory` of the static content is referred to as the `webroot`

During the certificate creation process, some challenge will be placed in the `webroot` for **each domain** and need to be accessible.

**Ex:** 
- If your webroot directory is `/webroot/`
- A challenge will be placed under `/webroot/.well-known/SOME_CHALLENGE`
- That challenge need to be accessible via `http://YOURDOMAIN/.well-known/SOME_CHALLENGE`
This is usually easily achieved with some FE-Server like `nginx`

#### Configuration

A configuration file needs to be **created** under `config.env` with the following variables:
- `WEBROOT`: Set the `webroot` directory (see #Prequisites)
- `LETSENCRYPT`: Path on your local machine where to store the generated `letsencrypt` folder
  - Certificates can be found under `LETSENCRYPT/live/YOURDOMAIN/` 
    - where `YOURDOMAIN` is the first domain from the `DOMAINS` list.
  - Since the certificates are renewed every 90 days the generated folder need to **NOT BE MOVED** after generation.
- `DOMAINS`: Domains for which to optain a certificate.
  - Separate domains with `space`: `DOMAINS="professionalbeginner.com www.professionalbeginner.com"`
  - Do not forget to add both `DOMAIN` **and** `www.DOMAIN` variant of your domain (if applicable)

A **template** is available under `config.env.template`

### Use

#### 1. First certificate generation

After following the steps from **#Prequisites**, run `./generate_new_certificate.sh`.

#### 2. Automatic renewal

**"Let's encrypt"** certificates expire after **90 days**. 
To avoid having to manually renew the certificates, run `./start_renew_daemon.sh`.

That will start a docker daemon that will renew the certificate automatically whenever needed.

**Do not move the certificate location**
(or start again the process from step 1.) 

If you ever need to stop the daemon, run `./stop_renew_daemon.sh`

#### X. Advanced Use
##### Run a custom `certbot` command

To run a custom `certbot` command, run `./run_custom_certbot_cmd.sh` followed by the command.

**Ex:**  
To display informations about the certificates: `./run_custom_certbot_cmd.sh certificates`
