#!/bin/bash

if [ "$@" ]; then
  echo "###########"
  echo "# WARNING #"
  echo "###########"
  echo "#"
  echo "# Custom command must be ran by EDITING this script."
  echo "#"
  echo "# This is due to the way the \`config.env\` file is loaded"
  echo "# There might have been a better way, but I couldn't find it at the time."
  echo "# Feel free to update"
  echo "#"
  echo "###########"

  exit 1;
fi


###############
# Load config #
###############
if [ ! -f ./config.env ]; then
    echo "Config file not found! See README.md"
    exit 1
fi
if [ -z "$WITH_ENV" ]; then
    export WITH_ENV=true
    # Relaunch script with config loaded
    exec bash -ac ". ./config.env; $0"
fi



##########################
#     CUSTOM COMMAND     #
##########################

#CMD="renew --force-renew"
CMD="renew --dry-run"

##########################
#  END - CUSTOM COMMAND  #
##########################



# Set container directories locations
IN_CONTAINER_LETSENCRYPT="/etc/letsencrypt/"
IN_CONTAINER_WEBROOT_CHALLENGE="/webroot_challenge/"

docker run -it --rm \
        -v "$WEBROOT:$IN_CONTAINER_WEBROOT_CHALLENGE" \
        -v "$LETSENCRYPT:$IN_CONTAINER_LETSENCRYPT" \
        certbot/certbot $CMD
