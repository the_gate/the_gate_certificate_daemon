#!/bin/bash

###############
# Load config #
###############
if [ ! -f ./config.env ]; then
    echo "Config file not found! See README.md"
    exit 1
fi
if [ -z "$WITH_ENV" ]; then
    export WITH_ENV=true
    # Relaunch script with config loaded
    exec bash -ac ". ./config.env; $0"
fi

docker rm -f $RENEW_DAEMON_CONTAINER_NAME > /dev/null


if [ $? == '0' ]; then
    echo "Daemon stopped"
else
    echo "Problem stopping the Renew Daemon"
fi
